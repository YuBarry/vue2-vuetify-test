// import AirportShuttleIcon from '@material-ui/icons/AirportShuttle';
import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

import "/node_modules/vuetify/dist/vuetify.min.css";
// import colors from 'vuetify/lib/util/colors'
Vue.use(Vuetify);

export default new Vuetify({
//  breakpoint: {
//     mobileBreakpoint: 'sm' // This is equivalent to a value of 960
//   },


breakpoint: {
    thresholds: {
      xs: 340,
      sm: 540,
      md: 800,
      lg: 1280,
    },
    scrollBarWidth: 24,
  },
  
  theme: {
   
    themes: {
      
      light: {
        primary: "#FFC107",
        secondary: "#1E2D56",
        accent: "#D9D9D9",
        error: "#E70023",
        info: "#2196F3",
        success: "#4CAF50",
        warning: "E70023",
        lightblue: "#2B3990",
        yellow: "#FDDF4A",
        pink: "#FF1976",
        orange: "#FF8657",
        magenta: "#C33AFC",
        darkblue: "#1E2D56",
        gray: "#909090",
        neutralgray: "#9BA6C1",
        green: "#92C70B",
        red: "#FF5c4E",
        darkblueshade: "#308DC2",
        lightgray: "#DCDCDC",
        lightpink: "#FFCFE3",
        white: "#FFFFFF",
        dark:"#35363A",
        waypoint:"#77D353",

      },
    },
     
  },
});
