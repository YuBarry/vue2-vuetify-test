import Vue from 'vue' 
import Router from 'vue-router'
import Login from '@/views/Login.vue'
import SearchPage from '@/views/SearchPage.vue'



Vue.use(Router)

export default new Router  ( {

routes:[
{  path:'/' , 
   name: 'login' ,
   component: Login 

 }, 
  
{  path:'/searchpage' , 
   name: 'searchpage' ,
   component: SearchPage

},

  ]
  
})